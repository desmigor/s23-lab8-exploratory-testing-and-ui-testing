# Lab 8 - Exploratory testing and UI

## Testing Dribbble

Website Link: [https://dribbble.com/](https://dribbble.com/)

## Test Tours:

`Legend: ✅ = Done`

### <u>Test tour 1</u>

For Selenium, this test was the one chosen

| test tour           | Test-1                                                 |
|---------------------|--------------------------------------------------------|
| object to be tested | Login in using email                                   |
| test duration       | 4 sec                                                  | 
| tester              | Igor Mpore                                             |

Protocol 1

| step | action                                                             | status |
|------|--------------------------------------------------------------------|--------|
| 1    | Navigated to the homepage                                          |   ✅   |
| 2    | Pressed the sign in button                                         |   ✅   |
| 3    | Added my email in the email input field                            |   ✅   |
| 4    | Added my password in the password input field                      |   ✅   |
| 5    | Clicked the Sign In button                                         |   ✅   |

### <u>Test tour 2</u>

| test tour           | Test-2                  |
|---------------------|-------------------------|
| object to be tested | View popular shots      |
| test duration       | 26 sec                  | 
| tester              | Igor Mpore              |

Protocol 2

| step | action                                                                 | status |
|------|------------------------------------------------------------------------|--------|
| 1    | Navigated to the homepage                                              |   ✅   |
| 2    | Pressed the sign in button                                             |   ✅   |
| 3    | Added my email in the email input field                                |   ✅   |
| 4    | Added my password in the password input field                          |   ✅   |
| 5    | Clicked the Sign In button                                             |   ✅   |
| 6    | Press the dropdown in the leftmost corner of the homepage              |   ✅   |
| 7    | select "Popular" in the options                                        |   ✅   |

### <u>Test tour 3</u>

| test tour           | Test-3                                 |
|---------------------|----------------------------------------|
| object to be tested | Search a specific tag                  |
| test duration       | 34 sec                                 | 
| tester              | Igor Mpore                             |

Protocol 3

| step | action                                                                 | status |
|------|------------------------------------------------------------------------|--------|
| 1    | Navigated to the homepage                                              |   ✅   |
| 2    | Pressed the sign in button                                             |   ✅   |
| 3    | Added my email in the email input field                                |   ✅   |
| 4    | Added my password in the password input field                          |   ✅   |
| 5    | Clicked the Sign In button                                             |   ✅   |
| 6    | Press the filter buttin in the rightmost corner of the homepage        |   ✅   |
| 7    | Type "notifications" in the Tags input field                           |   ✅   |
| 6    | Click the first best relevant suggested tags                           |   ✅   |


