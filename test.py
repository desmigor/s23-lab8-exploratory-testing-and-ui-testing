from selenium import webdriver
from selenium.webdriver.common.by import By

# initialize the driver
driver = webdriver.Chrome()

# navigate to the Dribbble homepage
driver.get('https://dribbble.com/')

# click the "Sign in" button
driver.find_element("link text", "Sign in").click()

# enter valid login email
driver.find_element(By.NAME, "login").send_keys("dribbble_email")

# enter valid password
driver.find_element(By.NAME, "password").send_keys("dribbble_password")

# click the "Sign In" button
driver.find_element(By.CLASS_NAME,"form-sub").click()

# verify that the user is logged in
assert 'Dribbble' in driver.title

# close the browser window
driver.quit()
